import React from 'react';

const AboutUs = () => (
  <div className="about-us">
      <p className="title">ABOUT US</p>
      <div className="image-description">
        <img
          src="images/global-business-graph-growth-finance-stock-market-concept.png"
          alt="global-business-graph-growth-finance-stock-market-concept.png"
        />
        <div className="description">
          <p className="subtitle">Our Approach</p>
          <p className="about-us-text">We conduct our business using in-depth analytical tools and financial modeling methodologies to provide unique insights to ensure optimal results for our clients.</p>
          <p className="about-us-text">We align our financial and business success with our clients to ensure value creation in the short and long term.</p>
          <p className="about-us-text">We create and maintain long-term relationships with global institutional investors, banks, family offices, and ultra-high net worth individuals.</p>
          <p className="about-us-text">We go the extra mile for our clients and partners to ensure the best possible results in every deal we conduct.</p>
        </div>
      </div>
    </div>
)

export default AboutUs;