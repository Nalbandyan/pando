import React from 'react';

const Business = () => (
  <div className="business-container">
      <div className="image-container">
        <div>
          <div className="text">
            <p className="first-text">Debt Brokering,Modernized</p>
            <p className="second-text">Pando is a leading, data-driven, AI-based commercial real estate capital broker, optimizing <br />
                capital providers' deal flow while minimizing commercial real estate financing costs.</p>
          </div>
          <img
            src="images/business-people-analyzing-statistics-financial-concept.png"
            alt=""
          />
        </div>
      </div>
      <div className="business-second-container">
        <div className="steps">
          <div className="first-step">
            <div className="step-count">
              <svg
                width="40"
                height="40"
                viewBox="0 0 40 40"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
              <circle
                cx="20"
                cy="20"
                r="20"
                fill="#B4945C"
              />
              <path
                d="M22.0801 27H19.2578V16.1211L15.8887 17.166V14.8711L21.7773 12.7617H22.0801V27Z"
                fill="white"
              />
              </svg>
            </div>
            <div>
              <p className="first-line-text">Submit One Loan Request</p>
              <p className="second-line-text">Save time, energy, and eliminate hassle.</p>
            </div>
          </div>
          <div className="second-step">
            <div className="step-count">
             <svg
               width="40"
               height="40"
               viewBox="0 0 40 40"
               fill="none"
               xmlns="http://www.w3.org/2000/svg"
             >
              <circle
                cx="20"
                cy="20"
                r="20"
                fill="#B4945C"
              />
              <path
                d="M24.9707 27H15.2246V25.0664L19.8242 20.1641C20.4557 19.474 20.9212 18.8717 21.2207 18.3574C21.5267 17.8431 21.6797 17.3548 21.6797 16.8926C21.6797 16.2611 21.5202 15.7663 21.2012 15.4082C20.8822 15.0436 20.4264 14.8613 19.834 14.8613C19.196 14.8613 18.6914 15.0827 18.3203 15.5254C17.9557 15.9616 17.7734 16.5378 17.7734 17.2539H14.9414C14.9414 16.388 15.1465 15.597 15.5566 14.8809C15.9733 14.1647 16.5592 13.6048 17.3145 13.2012C18.0697 12.791 18.9258 12.5859 19.8828 12.5859C21.3477 12.5859 22.4837 12.9375 23.291 13.6406C24.1048 14.3438 24.5117 15.3366 24.5117 16.6191C24.5117 17.3223 24.3294 18.0384 23.9648 18.7676C23.6003 19.4967 22.9753 20.3464 22.0898 21.3164L18.8574 24.7246H24.9707V27Z"
                fill="white"
              />
            </svg>

            </div>
            <div>
              <p className="first-line-text">Multiple Lenders Review</p>
              <p className="second-line-text">No longer are you limited by your debt brokers' "rolodex"</p>
            </div>
          </div>
          <div className="third-step">
            <div className="step-count">
            <svg
              width="40"
              height="40"
              viewBox="0 0 40 40"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
            <circle
              cx="20"
              cy="20"
              r="20"
              fill="#B4945C"
            />
              <path
                d="M18.0957 18.6406H19.5996C20.3158 18.6406 20.8464 18.4616 21.1914 18.1035C21.5365 17.7454 21.709 17.2702 21.709 16.6777C21.709 16.1048 21.5365 15.6589 21.1914 15.3398C20.8529 15.0208 20.3841 14.8613 19.7852 14.8613C19.2448 14.8613 18.7923 15.0111 18.4277 15.3105C18.0632 15.6035 17.8809 15.9876 17.8809 16.4629H15.0586C15.0586 15.7207 15.2572 15.0566 15.6543 14.4707C16.0579 13.8783 16.6178 13.416 17.334 13.084C18.0566 12.752 18.8509 12.5859 19.7168 12.5859C21.2207 12.5859 22.3991 12.9473 23.252 13.6699C24.1048 14.3861 24.5312 15.3757 24.5312 16.6387C24.5312 17.2897 24.3327 17.8887 23.9355 18.4355C23.5384 18.9824 23.0176 19.4023 22.373 19.6953C23.1738 19.9818 23.7695 20.4115 24.1602 20.9844C24.5573 21.5573 24.7559 22.2344 24.7559 23.0156C24.7559 24.2786 24.2936 25.291 23.3691 26.0527C22.4512 26.8145 21.2337 27.1953 19.7168 27.1953C18.2975 27.1953 17.1354 26.821 16.2305 26.0723C15.332 25.3236 14.8828 24.334 14.8828 23.1035H17.7051C17.7051 23.6374 17.9036 24.0736 18.3008 24.4121C18.7044 24.7507 19.1992 24.9199 19.7852 24.9199C20.4557 24.9199 20.9798 24.7441 21.3574 24.3926C21.7415 24.0345 21.9336 23.5625 21.9336 22.9766C21.9336 21.5573 21.1523 20.8477 19.5898 20.8477H18.0957V18.6406Z"
                fill="white"
              />
            </svg>

            </div>
            <div>
              <p className="first-line-text">Receive Loan Offers</p>
              <p className="second-line-text">Merit-based, rather than relationship-based dealmaking.</p>
            </div>
          </div>
        </div>
        <div className="button-container">
          <button>Learn More</button>
        </div>
      </div>
    </div>
)


export default Business;