import React from 'react';
import Swiper from "./Swiper";

const News = () => (
  <div className="news">
      <p className="title">NEWS</p>
      <div>
       <Swiper
         loop
         width={window.innerWidth - 20}
         pagination={{clickable: true}}
       >
         <div className="news-container">
          <div className="news-item">
            <img
              src="images/business-strategy-success-target-goals.png"
              alt="business-strategy-success-target-goals.png"
            />
            <div className="right-side">
              <p className="subtitle">Lorem Ipsum</p>
              <p className="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
              <div className="button-container">
                <button>Learn More</button>
              </div>
            </div>
          </div>
          <div className="news-item">
            <img
              src="images/handshake-close-up-executives.png"
              alt="handshake-close-up-executives.png"
            />
            <div className="right-side">
              <p className="subtitle">Lorem Ipsum</p>
              <p className="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
              <div className="button-container">
                <button>Learn More</button>
              </div>
            </div>
          </div>
          <div className="news-item">
            <img
              src="images/group-diverse-people-having-business-meeting.png"
              alt="group-diverse-people-having-business-meeting.png"
            />
            <div className="right-side">
              <p className="subtitle">Lorem Ipsum</p>
              <p className="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
              <div className="button-container">
                <button>Learn More</button>
              </div>
            </div>
          </div>
        </div>
         <div className="news-container">
          <div className="news-item">
            <img
              src="images/business-strategy-success-target-goals.png"
              alt="business-strategy-success-target-goals.png"
            />
            <div className="right-side">
              <p className="subtitle">Lorem Ipsum</p>
              <p className="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
              <div className="button-container">
                <button>Learn More</button>
              </div>
            </div>
          </div>
          <div className="news-item">
            <img
              src="images/handshake-close-up-executives.png"
              alt="handshake-close-up-executives.png"
            />
            <div className="right-side">
              <p className="subtitle">Lorem Ipsum</p>
              <p className="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
              <div className="button-container">
                <button>Learn More</button>
              </div>
            </div>
          </div>
          <div className="news-item">
            <img
              src="images/group-diverse-people-having-business-meeting.png"
              alt="group-diverse-people-having-business-meeting.png"
            />
            <div className="right-side">
              <p className="subtitle">Lorem Ipsum</p>
              <p className="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
              <div className="button-container">
                <button>Learn More</button>
              </div>
            </div>
          </div>
        </div>
         <div className="news-container">
          <div className="news-item">
            <img
              src="images/business-strategy-success-target-goals.png"
              alt="business-strategy-success-target-goals.png"
            />
            <div className="right-side">
              <p className="subtitle">Lorem Ipsum</p>
              <p className="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
              <div className="button-container">
                <button>Learn More</button>
              </div>
            </div>
          </div>
          <div className="news-item">
            <img
              src="images/handshake-close-up-executives.png"
              alt="handshake-close-up-executives.png"
            />
            <div className="right-side">
              <p className="subtitle">Lorem Ipsum</p>
              <p className="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
              <div className="button-container">
                <button>Learn More</button>
              </div>
            </div>
          </div>
          <div className="news-item">
            <img
              src="images/group-diverse-people-having-business-meeting.png"
              alt="group-diverse-people-having-business-meeting.png"
            />
            <div className="right-side">
              <p className="subtitle">Lorem Ipsum</p>
              <p className="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
              <div className="button-container">
                <button>Learn More</button>
              </div>
            </div>
          </div>
        </div>
       </Swiper>
        </div>
    </div>
)


export default News;