import React from 'react';
import { Swiper as SwiperModule, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, Pagination } from 'swiper';

import 'swiper/swiper.scss';
import 'swiper/components/pagination/pagination.scss';

SwiperCore.use([Navigation, Pagination]);

const Swiper = (props) => {
  SwiperCore.use([Navigation]);

  return (
    <SwiperModule
      spaceBetween={50}
      {...props}
    >
      <>
        {props.children.map((val, key) => {
            return (
              <SwiperSlide
                key={`${val.type}_${key}`}
              >
                {props.children[key]}
              </SwiperSlide>
            )
          }
        )}
      </>
    </SwiperModule>
  );
}

export default Swiper;




