import React, { useState } from 'react';
import Header from "./Header";
import Footer from "./Footer";

const Wrapper = (props) => {
  const [scrollY, setScrollY] = useState(0)

  const getData = () => {
    setScrollY(window.scrollY)
  }

  window.addEventListener('scroll', getData)
  return (
    <div className="wrapper">
      <Header scrollY={scrollY} />
      {props.children}
      <Footer />
    </div>
  )
}

export default Wrapper;