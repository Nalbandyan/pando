import Wrapper from "../components/Wrapper";
import Business from "../components/Business";
import AboutUs from "../components/AboutUs";
import OurServices from "../components/OurServices";
import News from "../components/News";

const Home = () => {
  return (
    <Wrapper>
      <Business />
      <AboutUs />
      <OurServices />
      <News />
    </Wrapper>
  )
}

export default Home;